package com.sjzc.search.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sjzc.domain.*;
import com.sjzc.enums.ExceptionEnum;
import com.sjzc.exception.LyException;
import com.sjzc.search.client.BrandClient;
import com.sjzc.search.client.CategoryClient;
import com.sjzc.search.client.GoodsClient;
import com.sjzc.search.client.SpecificationClient;
import com.sjzc.search.pojo.Goods;
import com.sjzc.search.pojo.SearchRequest;
import com.sjzc.search.pojo.SearchResult;
import com.sjzc.search.repository.GoodsRepository;
import com.sjzc.utils.JsonUtils;
import com.sjzc.vo.PageResult;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.LongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.FetchSourceFilter;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SearchService {

    @Autowired
    private BrandClient brandClient;
    @Autowired
    private CategoryClient categoryClient;
    @Autowired
    private GoodsClient goodsClient;
    @Autowired
    private SpecificationClient specificationClient;
    @Autowired
    private GoodsRepository repository;
    @Autowired
    private ElasticsearchTemplate template;

    public Goods buildGoods(Spu spu){

        //查询分类
        List<Category> categories = categoryClient.queryCategoryListById(Arrays.asList(spu.getCid1(), spu.getCid2(), spu.getCid3()));
        if(CollectionUtils.isEmpty(categories)){
            throw new LyException(ExceptionEnum.CATEGORY_LIST_ISEMPTY);
        }
        List<String> names = categories.stream().map(Category::getName).collect(Collectors.toList());
        //查询品牌
        Brand brand = brandClient.queryBrandById(spu.getBrandId());
        if(brand==null){
            throw new LyException(ExceptionEnum.BRANDS_NOT_FOUND);
        }
        //搜索字段
        String all = spu.getTitle() + StringUtils.join(names," ") + brand.getName();


        //sku价格的集合
        List<Sku> skuList = goodsClient.querySkuBySpuId(spu.getId());
        if(CollectionUtils.isEmpty(skuList)){
            throw new LyException(ExceptionEnum.SKU_IS_NOT_FIND);
        }
        List<Long> priceList = skuList.stream().map(Sku::getPrice).collect(Collectors.toList());


        //查询sku
        List<Map<String,Object>> skus = new ArrayList<>();
        for (Sku sku : skuList) {
            Map<String,Object> map = new HashMap<>();
            map.put("id",sku.getId());
            map.put("title",sku.getTitle());
            map.put("price",sku.getPrice());
            map.put("image",StringUtils.substringBefore(sku.getImages(),","));
            skus.add(map);
        }


        //查询规格参数
        List<SpecParam> params = specificationClient.queryParamByGid(null, spu.getCid3(), true);
        if(CollectionUtils.isEmpty(params)){
            throw new LyException(ExceptionEnum.PARAM_IS_NOT_FOUND);
        }
        //查询商品详情
        SpuDetail spuDetail = goodsClient.queryDetailsBySpuId(spu.getId());
        //获取通用规格参数
        Map<Long, String> genericSpec = JsonUtils.toMap(spuDetail.getGenericSpec(), Long.class, String.class);
        //获取特殊规格参数
        Map<Long, List<String>> specialSpec = JsonUtils
                .nativeRead(spuDetail.getSpecialSpec(), new TypeReference<Map<Long, List<String>>>() {});
        //规格参数,key为名字，值为值
        Map<String,Object> specs = new HashMap<>();
        for (SpecParam param : params) {
            //规格名称
            String key= param.getName();
            Object value="";
            //是否为通用规格
            if(param.getGeneric()){
                value=genericSpec.get(param.getId());
                //TODO===============================================处理成段时报错==========================================================================
//                //判断是否为数值类型
//                if(param.getNumeric()){
//                    //处理成段
//                    value = chooseSegment(value.toString(), param);
//                }
            }else {
                value=specialSpec.get(param.getId());
            }
            //存入map
            specs.put(key,value);
        }

        Goods goods = new Goods();
        goods.setBrandId(spu.getBrandId());
        goods.setCid1(spu.getCid1());
        goods.setCid2(spu.getCid2());
        goods.setCid3(spu.getCid3());
        goods.setCreateTime(spu.getCreateTime());
        goods.setSubTitle(spu.getSubTitle());
        goods.setId(spu.getId());
        goods.setAll(all);//  搜索字段，标题分类品牌规格
        goods.setPrice(priceList);//  设置sku价格的集合
        goods.setSkus(JsonUtils.toString(skus));// 获得所有sku的集合json格式
        goods.setSpecs(specs);// 获得所有可搜索的规格参数
        return goods;
    }
    private String chooseSegment(String value, SpecParam p) {
        double val = NumberUtils.toDouble(value);
        String result = "其它";
        // 保存数值段
        for (String segment : p.getSegments().split(",")) {
            String[] segs = segment.split("-");
            // 获取数值范围
            double begin = NumberUtils.toDouble(segs[0]);
            double end = Double.MAX_VALUE;
            if(segs.length == 2){
                end = NumberUtils.toDouble(segs[1]);
            }
            // 判断是否在范围内
            if(val >= begin && val < end){
                if(segs.length == 1){
                    result = segs[0] + p.getUnit() + "以上";
                }else if(begin == 0){
                    result = segs[1] + p.getUnit() + "以下";
                }else{
                    result = segment + p.getUnit();
                }
                break;
            }
        }
        return result;
    }


    /**
     * 搜索功能
     * @param request
     * @return
     */
    public PageResult<Goods> search(SearchRequest request){
        String key=request.getKey();
        //判断是否有搜索条件，若没有，返回null，不允许搜索全部商品
        if(StringUtils.isBlank(key)){
            return null;
        }
        //elasticsearch从0开始
        int page = request.getPage()-1;
        int size = request.getSize();
        //创建查询构造器
        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
        //对结果进行过滤
        builder.withSourceFilter(new FetchSourceFilter(new String[]{"id","subTitle","skus"},null));
        //分页
        builder.withPageable(PageRequest.of(page,size));
        //过滤
//        QueryBuilder baseQuery = QueryBuilders.matchQuery("all",key);
        QueryBuilder baseQuery = buildbasicQuery(request);
        builder.withQuery(baseQuery);
        //聚合品牌
        String brandAggName="brand_agg";
        //==========为啥是brandId,不是brand.getId
        builder.addAggregation(AggregationBuilders.terms(brandAggName).field("brandId"));
        //聚合分类
        String categoryAggName="category_agg";
        builder.addAggregation(AggregationBuilders.terms(categoryAggName).field("cid3"));
        //查询
//        Page<Goods> result = repository.search(builder.build());
        AggregatedPage<Goods> result = template.queryForPage(builder.build(), Goods.class);
        //解析分页结果
        long total = result.getTotalElements();
        int totalPage = result.getTotalPages();
        List<Goods> goodsList = result.getContent();
        //解析聚合结果
        Aggregations aggregations = result.getAggregations();
        List<Brand> brands =parseBrandAgg(aggregations.get(brandAggName));
        List<Category> categories=parseCategorryAgg(aggregations.get(categoryAggName));
        //聚合规格参数
        List<Map<String,Object>> specs = null;
        if(categories!=null && categories.size() == 1){
            specs=buildSpecificationAgg(categories.get(0).getId(),baseQuery);
        }




        return new SearchResult(total,totalPage,goodsList,categories,brands,specs);


    }

    //TODO 通过条件过滤商品wei实现,前台页面filter的过滤条件为放到filter{}中,55aoper
    private QueryBuilder buildbasicQuery(SearchRequest request) {
        //创建布尔查询
        BoolQueryBuilder builder = QueryBuilders.boolQuery();
//        查询条件
        builder.must(QueryBuilders.matchQuery("all",request.getKey()));
        //过滤条件
        Map<String, String> map = request.getFilter();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey();
            if(!"cid3".equals(key) && !"brandId".equals(key)){
                key="specs."+key+".keyword";
            }
            builder.filter(QueryBuilders.termQuery(key,entry.getValue()));
        }
        return builder;


    }

    private List<Map<String, Object>> buildSpecificationAgg(Long cid, QueryBuilder baseQuery) {
        List<Map<String,Object>> specs = new ArrayList<>();
        //查询需要聚合的规格参数
        List<SpecParam> params = specificationClient.queryParamByGid(null, cid, true);
        //聚合
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        //聚合是带上查询条件
        queryBuilder.withQuery(baseQuery);
        for (SpecParam param : params) {
            String name=param.getName();
            queryBuilder.addAggregation(AggregationBuilders.terms(name).field("specs."+name+".keyword"));
        }
        //获取结果
        AggregatedPage<Goods> result = template.queryForPage(queryBuilder.build(), Goods.class);
        //解析结果
        Aggregations aggregations = result.getAggregations();
        for (SpecParam param : params) {
            String name=param.getName();
            StringTerms terms = aggregations.get(name);
            List<String> options =
                    terms.getBuckets().stream().map(b -> b.getKeyAsString()).collect(Collectors.toList());
            //准备map
            Map<String,Object> map= new HashMap<>();
            map.put("k",name);
            map.put("options",options);
            specs.add(map);

        }
        return specs;
    }

    private List<Brand> parseBrandAgg(LongTerms terms) {
        try {
            List<Long> ids = terms.getBuckets().stream().map(b -> b.getKeyAsNumber().longValue())
                    .collect(Collectors.toList());
            List<Brand> brands = brandClient.queryBrandsByIds(ids);
            return brands;
        }catch(Exception e){
            return null;
        }
    }

    private List<Category> parseCategorryAgg(LongTerms terms) {
        try {
            List<Long> ids = terms.getBuckets().stream().map(b -> b.getKeyAsNumber().longValue())
                    .collect(Collectors.toList());
            List<Category> categories = categoryClient.queryCategoryListById(ids);
            return categories;
        }catch (Exception e){
            return null;
        }
    }

    public void createOrUpdateIndex(Long spuId) {
        Spu spu = goodsClient.querySpuById(spuId);
        Goods goods = buildGoods(spu);
        repository.save(goods);
    }

    public void deleteIndex(Long spuId) {
        repository.deleteById(spuId);
    }
}
