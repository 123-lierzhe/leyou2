package com.sjzc;

import com.sjzc.domain.Category;
import com.sjzc.search.client.CategoryClient;
import com.sjzc.search.pojo.Goods;
import com.sjzc.search.repository.GoodsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CategoryClientTest {

    @Autowired
    private CategoryClient categoryClient;
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
    @Autowired
    private GoodsRepository goodsRepository;

    @Test
    public void queryCategoryByIds(){
        List<Category> categories = categoryClient.queryCategoryListById(Arrays.asList(1L, 2L, 3L));
        for (Category category : categories) {
            System.out.println(category);
        }
    }

    @Test
    public void creatGoods(){
        elasticsearchTemplate.createIndex(Goods.class);
        elasticsearchTemplate.putMapping(Goods.class);
    }
}
