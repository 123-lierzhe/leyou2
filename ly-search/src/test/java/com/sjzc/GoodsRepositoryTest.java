package com.sjzc;

import com.sjzc.domain.Spu;
import com.sjzc.enums.ExceptionEnum;
import com.sjzc.exception.LyException;
import com.sjzc.search.client.GoodsClient;
import com.sjzc.search.pojo.Goods;
import com.sjzc.search.repository.GoodsRepository;
import com.sjzc.search.service.SearchService;
import com.sjzc.vo.PageResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
@RunWith(SpringRunner.class)
public class GoodsRepositoryTest {
    @Autowired
    private GoodsRepository goodsRepository;
    @Autowired
    private GoodsClient goodsClient;
    @Autowired
    private SearchService searchService;


    @Test
    public void loadData(){
        int page = 1;
        int rows = 100;
        int size = 0;
        do {
            //查询spu
            PageResult<Spu> result = goodsClient.querySpuByPage(null, true, page, rows);
            List<Spu> spuList = result.getItems();
            if (CollectionUtils.isEmpty(spuList)) {
                break;
            }

            //构建成goods
            List<Goods> goodlist = spuList.stream()
                    .map(searchService::buildGoods).collect(Collectors.toList());


            //存入索引库
            goodsRepository.saveAll(goodlist);
            //翻页
            page++;
            size = spuList.size();
        }while (size == 100);
    }
}
