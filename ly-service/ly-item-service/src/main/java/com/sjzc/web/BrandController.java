package com.sjzc.web;

import com.sjzc.domain.Brand;
import com.sjzc.service.BrandService;
import com.sjzc.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("brand")
public class BrandController {
    @Autowired
    private BrandService brandService;

    /**
     * 分类查询品牌并可对其进行排序搜索
     *
     *
     */
     @GetMapping("page")
    private ResponseEntity<PageResult<Brand>> queryBrandPage(
            @RequestParam(value="page",defaultValue = "1") Integer page,
            @RequestParam(value="rows",defaultValue = "5") Integer rows,
            @RequestParam(value="sortBy",required = false) String sortBy,
            @RequestParam(value="desc",defaultValue = "false") Boolean desc,
            @RequestParam(value="key",required = false) String  key
    ){
            PageResult<Brand> brandPageResult = brandService.queryBrandPage(page, rows, sortBy, desc, key);
            return ResponseEntity.ok(brandPageResult);
    }

    /**
     * 新增品牌
     *
     *
     *
     */
    @PostMapping
    public ResponseEntity<Void> saveBrand(Brand brand,@RequestParam("cids") List<Long> cids){
        brandService.saveBrand(brand,cids);
        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    /**
     *
     * 通过分类的id查询有哪些品牌
     *
     */
    @GetMapping("cid/{cid}")
    public ResponseEntity<List<Brand>> queryBrandsByCid(@PathVariable("cid") Long cid){
        return ResponseEntity.ok(brandService.queryBrandsByCid(cid));
    }

    /**
     * 通过id查询有品牌
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public ResponseEntity<Brand> queryBrandById(@PathVariable("id") Long id){
        return ResponseEntity.ok(brandService.queryById(id));
    }

    /**
     * 通过多个id一次查询多个品牌
     * @param ids
     * @return
     */
    @GetMapping("idList")
    public ResponseEntity<List<Brand>> queryBrandsByIds(@RequestParam("ids") List<Long> ids){
        return ResponseEntity.ok(brandService.queryBrandsByIds(ids));
    }

}
