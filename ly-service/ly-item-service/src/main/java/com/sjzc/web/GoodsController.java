package com.sjzc.web;

import com.sjzc.domain.Sku;
import com.sjzc.domain.Spu;
import com.sjzc.domain.SpuDetail;
import com.sjzc.dto.CartDTO;
import com.sjzc.service.GoodsService;
import com.sjzc.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class GoodsController {

    @Autowired
    private GoodsService goodsService;


    /**
     *
     *查询商品并进行排序，分页展示
     *
     */
    @GetMapping("/spu/page")
    public ResponseEntity<PageResult<Spu>> querySpuByPage(
            @RequestParam(value="key",required =false) String key,
            @RequestParam(value="saleable",required =false) Boolean saleable,
            @RequestParam(value="page",defaultValue = "1") Integer page,
            @RequestParam(value="rows",defaultValue = "5") Integer rows

    ){
        return ResponseEntity.ok(goodsService.querySpuBypage(key,saleable,page,rows));

    }

    /**
     *
     * 新增商品
     *
     */
    @PostMapping("goods")
    public ResponseEntity<Void> saveGoods(@RequestBody Spu spu){
        goodsService.saveGoods(spu);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     *
     * 修改啊商品
     *
     */
    @PutMapping("goods")
    public ResponseEntity<Void> updateGoods(@RequestBody Spu spu){
        goodsService.updateGoods(spu);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    /**
     *
     * 通过spu的id查询 detail
     *
     */
    @GetMapping("/spu/detail/{id}")
    public ResponseEntity<SpuDetail> queryDetailsBySpuId(@PathVariable("id") Long spuId){
        return ResponseEntity.ok(goodsService.queryDetailsBySpuId(spuId));
    }

    /**
     *
     * 通过spu的id查询 sku
     *
     */
    @GetMapping("/sku/list")
    public  ResponseEntity<List<Sku>> querySkuBySpuId(@RequestParam("id") Long spuId){
        return ResponseEntity.ok(goodsService.querySkuBySpuId(spuId));
    }

    // 未登录时cart.html不能正常渲染，在其请求到的方法中未找到该方法
    /**
     * 通过ids查询sku
     * @param ids
     * @return
     */
    @GetMapping("/sku/list/ids")
    public  ResponseEntity<List<Sku>> querySkuByIds(@RequestParam("ids") List<Long> ids){
        return ResponseEntity.ok(goodsService.querySkuByIds(ids));
    }

    /**
     * 根据id查询spu
     * @param id
     * @return
     */
    @GetMapping("/spu/{id}")
    public ResponseEntity<Spu> querySpuById(@PathVariable("id") Long id){
        return ResponseEntity.ok(goodsService.querySpuById(id));
    }

    @PostMapping("stock/decrease")
    public ResponseEntity<Void> decreaseStock(@RequestBody List<CartDTO> carts){
        goodsService.decreaseStock(carts);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
