package com.sjzc.web;

import com.sjzc.domain.SpecGroup;
import com.sjzc.domain.SpecParam;
import com.sjzc.service.SpecificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("spec")
public class SpecificationController {
    @Autowired
    private SpecificationService specificationService;

    /**
     *
     * 查询商品规格组名
     *
     */
    @GetMapping("groups/{cid}")
    public ResponseEntity<List<SpecGroup>> queryGroupByCid(@PathVariable("cid") Long cid){
        List<SpecGroup> groupList = specificationService.queryGroupByCid(cid);
        return ResponseEntity.ok(groupList);
//        return ResponseEntity.ok(specificationService.queryGroupByCid());
    }

    /**
     *
     * 根据组名或分类的id，或是否为查询条件查询参数
     *
     */
    @GetMapping("params")
    public ResponseEntity<List<SpecParam>> queryParamByGid(
            @RequestParam(value = "gid",required = false) Long gid,
            @RequestParam(value = "cid",required = false) Long cid,
            @RequestParam(value = "searching",required = false) Boolean  searching

    ){
        List<SpecParam> paramList = specificationService.queryParamsByGid(gid,cid,searching);
        return ResponseEntity.ok(paramList);
    }

    @GetMapping("/group")
    public ResponseEntity<List<SpecGroup>> queryListByCid(@RequestParam("cid") Long cid){
        return ResponseEntity.ok(specificationService.queryListByCid(cid));
    }
}
