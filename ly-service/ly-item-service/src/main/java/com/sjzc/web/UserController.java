package com.sjzc.web;

import com.sjzc.domain.User;
import com.sjzc.enums.ExceptionEnum;
import com.sjzc.exception.LyException;
import com.sjzc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;
    @PostMapping
    public ResponseEntity<User> addUser(User user){
        //校验
        if(user.getName()==null||user.getAge()==null){
//            1.输入数据有误失败返回500,出现异常什么也不反回
//                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
//            2.返回异常信息
            throw new LyException(ExceptionEnum.AGE_OR_NAME_CAN_NOT_NULL);
        }
        User user1 = userService.addUser(user);
//        增加用户成功返回201
        return ResponseEntity.status(HttpStatus.CREATED).body(user1);
    }
}
