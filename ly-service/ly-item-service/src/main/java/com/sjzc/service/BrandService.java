package com.sjzc.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sjzc.domain.Brand;
import com.sjzc.enums.ExceptionEnum;
import com.sjzc.exception.LyException;
import com.sjzc.mapper.BrandMapper;
import com.sjzc.vo.PageResult;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class BrandService {

    @Autowired
    private BrandMapper brandMapper;


    /**
     * 分类查询品牌并可对其进行排序搜索
     *
     *
     *
     */
    public PageResult<Brand> queryBrandPage(Integer page, Integer rows, String sortBy, Boolean desc, String key) {

        //对数据进行分页
        PageHelper.startPage(page,rows);

        //读取对应的数据库
        Example example = new Example(Brand.class);

        //给出搜索条件,进行过滤
        if(StringUtils.isNotBlank(key)){
            //如果搜索条件不为空，则匹配表中name相似的或者首字母相同的
            example.createCriteria().orLike("name","%"+key+"%")
                    .orEqualTo("letter",key.toUpperCase());
        }

        //给出排序方式,如果sortBY不为空，则通过sortBy顺序进行查询，其中顺序有其中的三元运算符进行控制
        if(StringUtils.isNotBlank(sortBy)){
            //特别注意下边空格的位置---------------------------------------------------------------
            String orderByClause = sortBy + (desc ? " DESC" : " ASC");
            example.setOrderByClause(orderByClause);
        }

        //搜索
        List<Brand> list = brandMapper.selectByExample(example);
        if(CollectionUtils.isEmpty(list)){
            throw new LyException(ExceptionEnum.BRANDS_NOT_FOUND);
        }

        //对list中的list队形进行强转，并获得其中的总条数
        PageInfo<Brand> info = new PageInfo<>(list);
//        返回总条数以查询列表
        return new PageResult<>(info.getTotal(),list);
    }



    /**
     * 新增品牌
     *
     *
     *
     */
    @Transactional
    public void saveBrand(Brand brand, List<Long> cids) {

        //先新增商品牌,因为品牌id自增，先置空
        brand.setId(null);
        int insert = brandMapper.insert(brand);
        if(insert !=1){
            throw new LyException(ExceptionEnum.SAVE_BRAND_FAILED);
        }

        //在对中间表进行插入数据
        for(Long cid: cids){
            int i = brandMapper.insertCategoryBrand(cid, brand.getId());
            if(i!=1){
                throw new LyException(ExceptionEnum.SAVE_BRAND_FAILED);
            }

        }

    }

    /**
     *
     * 通过id查询品牌
     *
     */
    public Brand queryById(Long id){
        Brand brand = brandMapper.selectByPrimaryKey(id);
        if(brand==null){
            throw new LyException(ExceptionEnum.BRANDS_NOT_FOUND);
        }
        return brand;
    }

    /**
     *
     * 通过分类的id查询有哪些品牌
     *
     */
    public List<Brand> queryBrandsByCid(Long cid) {
        List<Brand> list = brandMapper.queryBrandsByCid(cid);
        if(CollectionUtils.isEmpty(list)){
            throw new LyException(ExceptionEnum.BRANDS_NOT_FOUND);
        }
        return list;
    }

    public List<Brand> queryBrandsByIds(List<Long> ids) {
        List<Brand> list = brandMapper.selectByIdList(ids);
        if(CollectionUtils.isEmpty(list)){
            throw new LyException(ExceptionEnum.BRANDS_NOT_FOUND);
        }
        return list;
    }
}
