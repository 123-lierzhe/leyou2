package com.sjzc.service;

import com.sjzc.domain.SpecGroup;
import com.sjzc.domain.SpecParam;
import com.sjzc.enums.ExceptionEnum;
import com.sjzc.exception.LyException;
import com.sjzc.mapper.SpecGroupMapper;
import com.sjzc.mapper.SpecParamMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SpecificationService {
    @Autowired
    private SpecGroupMapper specGroupMapper;
    @Autowired
    private SpecParamMapper specParamMapper;

    /**
     * 根据cid查找组
     * @param cid
     * @return
     */
    public List<SpecGroup> queryGroupByCid(Long cid) {
        SpecGroup s = new SpecGroup();
        s.setCid(cid);
        List<SpecGroup> select = specGroupMapper.select(s);
        if(CollectionUtils.isEmpty(select)){
            throw new LyException(ExceptionEnum.GROUP_IS_NOT_FOUND);
        }

        return select;
    }

    /**
     *
     * 根据组名或分类的id，或是否为查询条件查询参数
     *
     */
    public List<SpecParam> queryParamsByGid(Long gid, Long cid, Boolean searching) {
        SpecParam s = new SpecParam();
        s.setGroupId(gid);
        s.setCid(cid);
        s.setSearching(searching);
        List<SpecParam> select = specParamMapper.select(s);
        if(CollectionUtils.isEmpty(select)){
            throw new LyException(ExceptionEnum.PARAM_IS_NOT_FOUND);
        }
        return select;
    }

    public List<SpecGroup> queryListByCid(Long cid) {
        //查询组
        List<SpecGroup> specGroups = queryGroupByCid(cid);
        //查询所有的参数
        List<SpecParam> specParams = queryParamsByGid(null, cid, null);
        //吧规格参数变成map，key为group的id，value为对应的params
        Map<Long,List<SpecParam>> map = new HashMap<>();
        for (SpecParam specParam : specParams) {
            if(!map.containsKey(specParam.getGroupId())){
                map.put(specParam.getGroupId(),new ArrayList<>());
            }
            map.get(specParam.getGroupId()).add(specParam);
        }
        //填充param到group
        for (SpecGroup specGroup : specGroups) {
            specGroup.setParams(map.get(specGroup.getId()));
        }
        return specGroups;
    }
}
