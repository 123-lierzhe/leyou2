package com.sjzc.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sjzc.domain.*;
import com.sjzc.dto.CartDTO;
import com.sjzc.enums.ExceptionEnum;
import com.sjzc.exception.LyException;
import com.sjzc.mapper.SkuMapper;
import com.sjzc.mapper.SpuDetailMapper;
import com.sjzc.mapper.SpuMapper;
import com.sjzc.mapper.StockMapper;
import com.sjzc.vo.PageResult;
import org.apache.commons.lang.StringUtils;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GoodsService {

    @Autowired
    private SpuMapper spuMapper;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private SpuDetailMapper spuDetailMapper;

    @Autowired
    private SkuMapper skuMapper;

    @Autowired
    private StockMapper stockMapper;

    @Autowired
    private AmqpTemplate amqpTemplate;

    /**
     *
     * 查询商品并进行排序，分页展示
     *
     */
    public PageResult<Spu> querySpuBypage(String key, Boolean saleable, Integer page, Integer rows) {
        //分页
        PageHelper.startPage(page,rows);
        //过滤
        Example example = new Example(Spu.class);
        if(StringUtils.isNotBlank(key)){
            example.createCriteria().andLike("title","%"+ key +"%");
        }
        if(saleable != null){
            example.createCriteria().andEqualTo("saleable", saleable);
        }
        //默认排序
        example.setOrderByClause("id DESC");
        //查询
        List<Spu> spus = spuMapper.selectByExample(example);
        if(CollectionUtils.isEmpty(spus)){
            throw new LyException(ExceptionEnum.GOODS_NOT_FIND);
        }
        //将id转换为字符串
        loadCategoryAndBrandNmae(spus);
        //解析
        PageInfo<Spu> list = new PageInfo<>(spus);

        return new PageResult<>(list.getTotal(),spus);
    }

    /**
     *
     * 经品牌，分类由id转为对应的名字字符串，
     *
     */
    private void loadCategoryAndBrandNmae(List<Spu> spus){
        for(Spu spu:spus){
            //处理分类名称
            List<String> names = categoryService.queryByIds(Arrays.asList(spu.getCid1(), spu.getCid2(), spu.getCid3()))
                    .stream().map(Category::getName).collect(Collectors.toList());
            spu.setCname(StringUtils.join(names,"/"));
            //处理品牌名称
            Brand brand = brandService.queryById(spu.getBrandId());
            spu.setBname(brand.getName());
        }

    }

    /**
     *
     * 新增商品
     *
     */
    @Transactional
    public void saveGoods(Spu spu) {
        //添加spu
        spu.setId(null);
        spu.setCreateTime(new Date());
        spu.setLastUpdateTime(spu.getCreateTime());
        spu.setSaleable(true);
        spu.setValid(false);
        int count = spuMapper.insert(spu);
        if(count!=1){
            throw new LyException(ExceptionEnum.SAVE_GOODS_ERROR);
        }
        //添加spuDetails
        SpuDetail spuDetail = spu.getSpuDetail();
        spuDetail.setSpuId(spu.getId());
        spuDetailMapper.insert(spuDetail);
        //定义库存集合使其能够一步添加
        List<Stock> stockList = new ArrayList<>();
        //添加sku
        List<Sku> skus = spu.getSkus();
        for(Sku sku:skus){
            sku.setCreateTime(new Date());
            sku.setLastUpdateTime(sku.getCreateTime());
            sku.setSpuId(spu.getId());
            int count1 = skuMapper.insert(sku);
            if(count1!=1){
                throw new LyException(ExceptionEnum.SAVE_GOODS_ERROR);
            }
/*           1 //逐个添加库存
            Stock stock = new Stock();
            stock.setSkuId(sku.getId());
            stock.setStock(sku.getStock());
            int insert = stockMapper.insert(stock);
            if(insert!=1){
                throw  new LyException(ExceptionEnum.SAVE_GOODS_ERROR);
            }*/
            //2一次添加多个库存
            Stock stock = new Stock();
            stock.setSkuId(sku.getId());
            stock.setStock(sku.getStock());

            stockList.add(stock);
        }
        //2一次添加多个库存
        int i = stockMapper.insertList(stockList);
        if(i!=1){
            throw new LyException(ExceptionEnum.SAVE_GOODS_ERROR);
        }
        //发送mq消息
        amqpTemplate.convertAndSend("item.insert",spu.getId());
    }

    /**
     *
     * 修改商品
     *
     */
    @Transactional
    public void updateGoods(Spu spu) {
        //查询spu是否存在
        if(spu.getId()==null){
            throw new LyException(ExceptionEnum.GOODS_ID_IS_NOT_FIND);
        }
        //查询sku
        Sku sku = new Sku();
        sku.setSpuId(spu.getId());
        List<Sku> select = skuMapper.select(sku);
        if(!CollectionUtils.isEmpty(select)){
            //删除sku
            skuMapper.delete(sku);
            //删除库存
            List<Long> ids = select.stream().map(Sku::getId).collect(Collectors.toList());
            stockMapper.deleteByIdList(ids);
        }
        //修改spu
        spu.setValid(null);
        spu.setSaleable(null);
        spu.setCreateTime(null);
        spu.setLastUpdateTime(new Date());
        int i = spuMapper.updateByPrimaryKeySelective(spu);
        if(i != 1){
            throw new LyException(ExceptionEnum.UPDATE_SPU_ERROR);
        }
        //修改detail
        int i1 = spuDetailMapper.updateByPrimaryKeySelective(spu.getSpuDetail());
        if(i1 != 1){
            throw new LyException(ExceptionEnum.UPDATE_SPU_ERROR);
        }
        //新增sku和库存
        //定义库存集合使其能够一步添加
        List<Stock> stockList = new ArrayList<>();
        //添加sku
        List<Sku> skus = spu.getSkus();
        for(Sku sku2:skus){
            sku2.setCreateTime(new Date());
            sku2.setLastUpdateTime(sku2.getCreateTime());
            sku2.setSpuId(spu.getId());
            int count1 = skuMapper.insert(sku2);
            if(count1!=1){
                throw new LyException(ExceptionEnum.SAVE_GOODS_ERROR);
            }

            //2一次添加多个库存
            Stock stock = new Stock();
            stock.setSkuId(sku2.getId());
            stock.setStock(sku2.getStock());

            stockList.add(stock);
        }
        //2一次添加多个库存
        int i3 = stockMapper.insertList(stockList);
        if(i3 !=1){
            throw new LyException(ExceptionEnum.SAVE_GOODS_ERROR);
        }
        //发送mq消息
        amqpTemplate.convertAndSend("item.update",spu.getId());
    }

    /**
     *
     * 通过spu的id查询 detail
     *
     */
    public SpuDetail queryDetailsBySpuId(Long spuId) {
        SpuDetail spuDetail = spuDetailMapper.selectByPrimaryKey(spuId);
        if(spuDetail==null){
            throw new LyException(ExceptionEnum.FIND_FETAILS_ERROR);
        }
        return spuDetail;
    }

    /**
     *
     * 通过spu的id查询 sku
     *
     */
    public List<Sku> querySkuBySpuId(Long spuId) {
        //查询sku
        Sku sku = new Sku();
        sku.setSpuId(spuId);
        List<Sku> skuList = skuMapper.select(sku);
        if(CollectionUtils.isEmpty(skuList)){
            throw new LyException(ExceptionEnum.FIND_SKU_ERROR);
        }

        //cha查询库存
        for(Sku s:skuList ){
            Stock stock = stockMapper.selectByPrimaryKey(s.getId());
            if(stock==null){
                throw new LyException(ExceptionEnum.FIND_STOCK_ERROR);
            }
            s.setStock(stock.getStock());

        }
        return skuList;
    }

    /**
     * 根据id查询spu
     * @param id
     * @return
     */
    public Spu querySpuById(Long id) {
        //查询spu
        Spu spu = spuMapper.selectByPrimaryKey(id);
        if(spu == null){
            throw new LyException(ExceptionEnum.GOODS_NOT_FIND);
        }
        //查询sku
        spu.setSkus(querySkuBySpuId(id));
        //查询detail
        spu.setSpuDetail(queryDetailsBySpuId(id));
        return spu;
    }

    public List<Sku> querySkuByIds(List<Long> ids) {
        List<Sku> skuList = skuMapper.selectByIdList(ids);
        if(CollectionUtils.isEmpty(skuList)){
            throw new LyException(ExceptionEnum.GOODS_NOT_FIND);
        }

        //cha查询库存
        for(Sku s:skuList ){
            Stock stock = stockMapper.selectByPrimaryKey(s.getId());
            if(stock==null){
                throw new LyException(ExceptionEnum.FIND_STOCK_ERROR);
            }
            s.setStock(stock.getStock());

    }
        return skuList;
    }

    @Transactional
    public void decreaseStock(List<CartDTO> carts) {
        for (CartDTO cart : carts) {
            int count=stockMapper.decreaseStock(cart.getSkuId(),cart.getNum());
            if(count!=1){
                throw new LyException(ExceptionEnum.STOCH_NOT_ENOUGH);
            }
        }
    }
}
