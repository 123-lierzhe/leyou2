package com.sjzc.service;

import com.sjzc.domain.Category;
import com.sjzc.enums.ExceptionEnum;
import com.sjzc.exception.LyException;
import com.sjzc.mapper.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    /**
     *
     * 通过pid查询分类
     *
     */
    public List<Category> queryCategoryListByPid(Long pid) {
        //通过不是主键查询商品的方法，定义一个category，对其中的哪个属性赋值，则会根据那个属性进行查询
        Category c = new Category();
        c.setParentId(pid);
        List<Category> list = categoryMapper.select(c);
        //判断查询的结果是否为空，为空抛出异常
        if(CollectionUtils.isEmpty(list)){
            throw new LyException(ExceptionEnum.CATEGORY_LIST_ISEMPTY);
        }
        return list;
    }

    /**
     *
     * 一次通过多个进行查询
     *
     */
    public List<Category> queryByIds(List<Long> ids){
        List<Category> categories = categoryMapper.selectByIdList(ids);
        if(CollectionUtils.isEmpty(categories)){
            throw  new LyException(ExceptionEnum.CATEGORY_LIST_ISEMPTY);
        }
        return categories;
    }
}
