package com.sjzc.mapper;

import com.sjzc.domain.Brand;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.RequestParam;
import tk.mybatis.mapper.additional.idlist.IdListMapper;
import tk.mybatis.mapper.common.BaseMapper;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface BrandMapper extends Mapper<Brand>, IdListMapper<Brand,Long> {

    /**
     * 新增品牌是对中间表插入数据
     *
     *
     */
    @Insert("insert into tb_category_brand (category_id,brand_id) values (#{cid},#{bid})")
    public int insertCategoryBrand(@RequestParam("cid") Long cid,@RequestParam("bid") Long bid);

    /**
     *
     * 通过分类的id查询有哪些品牌
     *
     */
    @Select("select b.* from tb_brand b left join tb_category_brand cb on b.id = cb.brand_id where cb.category_id = #{cid}")
    public List<Brand> queryBrandsByCid(@Param("cid") Long cid);


}
