package com.sjzc.mapper;

import com.sjzc.domain.Stock;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.additional.idlist.DeleteByIdListMapper;
import tk.mybatis.mapper.additional.insert.InsertListMapper;
import tk.mybatis.mapper.common.Mapper;

public interface StockMapper extends Mapper<Stock>, InsertListMapper<Stock>, DeleteByIdListMapper<Stock,Long> {

    @Update("update tb_stock set stock = stock - #{num} where sku_id = #{id} and stock >=#{num}")
    int decreaseStock(@Param("id") Long id,@Param("num") Integer num);
}
