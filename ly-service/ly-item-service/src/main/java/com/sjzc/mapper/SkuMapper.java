package com.sjzc.mapper;

import com.sjzc.domain.Brand;
import com.sjzc.domain.Sku;
import tk.mybatis.mapper.additional.idlist.IdListMapper;
import tk.mybatis.mapper.common.Mapper;

public interface SkuMapper extends Mapper<Sku>, IdListMapper<Sku,Long> {
}
