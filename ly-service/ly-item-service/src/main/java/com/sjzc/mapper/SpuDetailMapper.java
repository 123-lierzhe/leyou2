package com.sjzc.mapper;

import com.sjzc.domain.SpuDetail;
import tk.mybatis.mapper.common.Mapper;

public interface SpuDetailMapper extends Mapper<SpuDetail> {
}
