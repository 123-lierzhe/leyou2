package com.sjzc.mapper;

import com.sjzc.domain.Spu;
import tk.mybatis.mapper.common.Mapper;

public interface SpuMapper extends Mapper<Spu> {
}
