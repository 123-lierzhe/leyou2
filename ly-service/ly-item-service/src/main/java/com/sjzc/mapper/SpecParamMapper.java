package com.sjzc.mapper;

import com.sjzc.domain.SpecParam;
import tk.mybatis.mapper.common.Mapper;

public interface SpecParamMapper extends Mapper<SpecParam> {
}
