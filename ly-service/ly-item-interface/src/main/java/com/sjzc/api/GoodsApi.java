package com.sjzc.api;

import com.sjzc.domain.Sku;
import com.sjzc.domain.Spu;
import com.sjzc.domain.SpuDetail;
import com.sjzc.dto.CartDTO;
import com.sjzc.vo.PageResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface GoodsApi {
    @GetMapping("/spu/page")
    public PageResult<Spu> querySpuByPage(
            @RequestParam(value="key",required =false) String key,
            @RequestParam(value="saleable",required =false) Boolean saleable,
            @RequestParam(value="page",defaultValue = "1") Integer page,
            @RequestParam(value="rows",defaultValue = "5") Integer rows

    );
    @GetMapping("/spu/detail/{id}")
    SpuDetail queryDetailsBySpuId(@PathVariable("id") Long spuId);
    @GetMapping("/sku/list")
    List<Sku> querySkuBySpuId(@RequestParam("id") Long spuId);
    @GetMapping("/spu/{id}")
    Spu querySpuById(@PathVariable("id") Long id);
    @GetMapping("/sku/list/ids")
    List<Sku> querySkuByIds(@RequestParam("ids") List<Long> ids);
    @PostMapping("stock/decrease")
    void decreaseStock(@RequestBody List<CartDTO> carts);

}
