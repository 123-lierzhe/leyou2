package com.sjzc.authtest;

import com.sjzc.entity.UserInfo;
import com.sjzc.utils.JwtUtils;
import com.sjzc.utils.RsaUtils;
import org.junit.Before;
import org.junit.Test;

import java.security.PrivateKey;
import java.security.PublicKey;

public class JwtTest {

    private static final String pubKeyPath = "E:\\workSpace Idea\\rsa.pub";

    private static final String priKeyPath = "E:\\workSpace Idea\\rsa.pri";

    private PublicKey publicKey;

    private PrivateKey privateKey;

    @Test
    public void testRsa() throws Exception {
        RsaUtils.generateKey(pubKeyPath, priKeyPath, "234");
    }

    //@Before
    public void testGetRsa() throws Exception {
        this.publicKey = RsaUtils.getPublicKey(pubKeyPath);
        this.privateKey = RsaUtils.getPrivateKey(priKeyPath);
    }

    @Test
    public void testGenerateToken() throws Exception {
        // 生成token
        String token = JwtUtils.generateToken(new UserInfo(20L, "jack"), privateKey, 5);
        System.out.println("token = " + token);
    }

    @Test
    public void testParseToken() throws Exception {
        String token = "eyJhbGciOiJSUzI1NiJ9.eyJpZCI6MjAsInVzZXJuYW1lIjoiamFjayIsImV4cCI6MTU5MTIzNzgzN30.EMcd3vO8TC3Od7zN5VFcVwg2Q-5wIUOXTB8mjaDwN3CxdvzdDSpRYUOmHJcquZJE2t0lK0rzq6Bs-PzAz1gVfI-bt1fKhMmqHryNLSPrV645W6aQclZWSkgRQaWicojumt_zWvMVSGPwkEGYc7vMsi5qLlJ581M9yfNAvIB0OJQ";

        // 解析token
        UserInfo user = JwtUtils.getInfoFromToken(token, publicKey);
        System.out.println("id: " + user.getId());
        System.out.println("userName: " + user.getUsername());
    }
}