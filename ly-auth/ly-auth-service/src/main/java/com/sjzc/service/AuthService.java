package com.sjzc.service;

import com.sjzc.client.UserClient;
import com.sjzc.config.JwtProperties;
import com.sjzc.entity.UserInfo;
import com.sjzc.enums.ExceptionEnum;
import com.sjzc.exception.LyException;
import com.sjzc.pojo.User;
import com.sjzc.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;
@Slf4j
@Service
@EnableConfigurationProperties(JwtProperties.class)
public class AuthService {
    @Autowired
    private UserClient userClient;
    @Autowired
    private JwtProperties jwtProperties;

    public String login(String username, String password) {
        try {
            //校验
            User user = userClient.queryUserByUsernameAndPassword(username, password);
            //判断
            if (user == null) {
                throw new LyException(ExceptionEnum.INCALID_USERNAME_PASSWORD);
            }
            //生成token
            String token = JwtUtils.generateToken(new UserInfo(user.getId(), password), jwtProperties.getPrivateKey(), jwtProperties.getExpire());


            return token;
        }catch (Exception e){
            log.info("【授权中心】用户名或密码错误，用户名称:{}",username,e);
            throw new LyException(ExceptionEnum.INCALID_USERNAME_PASSWORD);
        }
    }
}
