package com.sjzc.web;

import com.sjzc.config.JwtProperties;
import com.sjzc.entity.UserInfo;
import com.sjzc.enums.ExceptionEnum;
import com.sjzc.exception.LyException;
import com.sjzc.service.AuthService;
import com.sjzc.utils.CookieUtils;
import com.sjzc.utils.JwtUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@EnableConfigurationProperties(JwtProperties.class)
public class AuthController {
    @Autowired
    private JwtProperties prop;
    @Autowired
    private AuthService authService;

    /**
     * 登录
     * @param username
     * @param password
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity<String> login(
            @RequestParam("username") String username, @RequestParam("password") String password,
            HttpServletRequest request, HttpServletResponse response){
        //登录
      String token = authService.login(username,password);
        // 将token写入cookie,并指定httpOnly为true，防止通过JS获取和修改
        CookieUtils.setCookie(request,response,prop.getCookieName(),token);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

    }

    /**
     * 校验是否登录
     * @param token
     * @return
     */
    @GetMapping("/verify")
    public ResponseEntity<UserInfo> verify(@CookieValue("LY_TOKEN") String token,
                                           HttpServletRequest request,HttpServletResponse response){
        if(StringUtils.isBlank(token)){
            throw new LyException(ExceptionEnum.UNAUTHORIZED);
        }
        try {
            //解析token
            UserInfo user = JwtUtils.getInfoFromToken(token, prop.getPublicKey());
            //刷新token，重新写入token，防止过期时间需要重新登录
            String newToken = JwtUtils.generateToken(user, prop.getPrivateKey(), prop.getExpire());
            // 将token写入cookie,并指定httpOnly为true，防止通过JS获取和修改
            CookieUtils.setCookie(request,response,prop.getCookieName(),token);
            return ResponseEntity.ok(user);
        }catch (Exception e){
            throw new LyException(ExceptionEnum.UNAUTHORIZED);
        }

    }

}
