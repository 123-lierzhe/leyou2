package com.sjzc.service;

import com.sjzc.entity.UserInfo;
import com.sjzc.enums.ExceptionEnum;
import com.sjzc.exception.LyException;
import com.sjzc.interceptor.UserInterceptor;
import com.sjzc.pojo.Cart;
import com.sjzc.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CartService {
    @Autowired
    private StringRedisTemplate template;

    public static final String KEY_PREFIX="cart:uid:";

    public void addCart(Cart cart) {
        //获取登录用户
        UserInfo user = UserInterceptor.getUser();
        String key = KEY_PREFIX + user.getId();
        String hashKey = cart.getSkuId().toString();
        BoundHashOperations<String, Object, Object> operation = template.boundHashOps(key);
        //判断购物车当前商品是否存在
        if(operation.hasKey(hashKey)){
            //存在数量改变
            String json = operation.get(hashKey).toString();
            Cart cacheCart = JsonUtils.toBean(json, Cart.class);
            cacheCart.setNum(cacheCart.getNum()+cart.getNum());
            //写入redis
            operation.put(hashKey,JsonUtils.toString(cacheCart));
        }else {
            //不存在新增
            operation.put(hashKey,JsonUtils.toString(cart));


        }
    }

    public List<Cart> queryCartList() {
        //获取登录用户
        UserInfo user = UserInterceptor.getUser();
        String key = KEY_PREFIX + user.getId();
        //key不存在，返回404
        if(!template.hasKey(key)){
            throw new LyException(ExceptionEnum.CART_NOT_FOUND);
        }
        //获取登录用户的购物车
        BoundHashOperations<String, Object, Object> operation = template.boundHashOps(key);
        List<Cart> carts = operation.values().stream()
                .map(o -> JsonUtils.toBean(o.toString(), Cart.class))
                .collect(Collectors.toList());
        return carts;


    }

    public void updateCart(Long skuId, Integer num) {
        //获取登录用户
        UserInfo user = UserInterceptor.getUser();
        String key = KEY_PREFIX + user.getId();
        String hashKey = skuId.toString();
        BoundHashOperations<String, Object, Object> operations = template.boundHashOps(key);
        //判断是否存在
        if(!operations.hasKey(hashKey)){
            throw new LyException(ExceptionEnum.CART_NOT_FOUND);
        }
        //查询购物车
        Cart cart = JsonUtils.toBean(operations.get(hashKey).toString(), Cart.class);
        cart.setNum(num);
        //写回redis
        operations.put(hashKey,JsonUtils.toString(cart));
    }

    public void delateCart(Long skuId) {
        //获取登录用户
        UserInfo user = UserInterceptor.getUser();
        String key = KEY_PREFIX + user.getId();
        //删除
        template.opsForHash().delete(key,skuId.toString());
    }
}
