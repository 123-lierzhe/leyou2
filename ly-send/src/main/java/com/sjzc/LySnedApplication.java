package com.sjzc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LySnedApplication {
    public static void main(String[] args) {
        SpringApplication.run(LySnedApplication.class);
    }
}
