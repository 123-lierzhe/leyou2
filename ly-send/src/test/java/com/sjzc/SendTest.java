package com.sjzc;

import com.sjzc.config.SmsProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SendTest {
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Test
    public void send1Test() throws InterruptedException {
        Map<String,String> msg = new HashMap<>();
        msg.put("phone","19831142137");
        msg.put("code","12345");
        amqpTemplate.convertAndSend("ly.sms.exchange","sms.verify.code",msg);
        Thread.sleep(10000L);
    }

}
