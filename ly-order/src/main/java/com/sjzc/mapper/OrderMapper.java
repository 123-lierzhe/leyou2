package com.sjzc.mapper;

import com.sjzc.pojo.Order;
import tk.mybatis.mapper.common.Mapper;

public interface OrderMapper extends Mapper<Order> {
}
