package com.sjzc.mapper;

import com.sjzc.pojo.OrderStatus;
import tk.mybatis.mapper.common.Mapper;

public interface OrderStatusMapper extends Mapper<OrderStatus> {
}
