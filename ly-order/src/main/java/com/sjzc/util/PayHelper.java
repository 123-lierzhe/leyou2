package com.sjzc.util;

import com.github.wxpay.sdk.WXPay;
import com.github.wxpay.sdk.WXPayConstants;
import com.github.wxpay.sdk.WXPayUtil;
import com.sjzc.config.PayConfig;
import com.sjzc.enums.ExceptionEnum;
import com.sjzc.exception.LyException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
@Slf4j
@Component
public class PayHelper {



    @Autowired
    private WXPay wxPay;

    @Autowired
    private PayConfig config;


    public String createPayUrl(Long orderId,Long totalPay) {


        try {
            Map<String, String> data = new HashMap<>();
            // 商品描述
            data.put("body", "乐优商城商品支付");
            // 订单号
            data.put("out_trade_no", orderId.toString());
            //货币
            data.put("fee_type", "CNY");
            //金额，单位是分
            data.put("total_fee", totalPay.toString());
            //调用微信支付的终端IP（乐优商城的IP）
            data.put("spbill_create_ip", "127.0.0.1");
            //回调地址，付款成功后的接口
            data.put("notify_url", config.getNotifyUrl());
            // 交易类型为扫码支付
            data.put("trade_type", "NATIVE");
//            //商品id,使用假数据
//            data.put("product_id", "1234567");

            Map<String, String> result = wxPay.unifiedOrder(data);

            if ("FAIL".equals(result.get("return_code"))) {
                log.error("【微信下单】微信下单通信失败，失败原因：{}",result.get("return_msg"));
                throw new LyException(ExceptionEnum.WX_PAY_ORDER_FAIL);
            }

            if ("FAIL".equals(result.get("result_code"))) {
                log.error("【微信下单】微信下单业务失败");
                throw new LyException(ExceptionEnum.WX_PAY_ORDER_FAIL);
            }

                String url = result.get("code_url");
                return url;
        } catch (Exception e) {
            log.error("创建预交易订单异常", e);
            return null;
        }
    }

    public void isValidSign(Map<String, String> data) {
        try {
            String sign1 = WXPayUtil.generateSignature(data, config.getKey(), WXPayConstants.SignType.HMACSHA256);
            String sign2 = WXPayUtil.generateSignature(data, config.getKey(), WXPayConstants.SignType.MD5);

            //与传过来的签名进行比较
            String sign = data.get("sign");
            if (!StringUtils.equals(sign, sign1) && !StringUtils.equals(sign, sign2)) {
                throw new LyException(ExceptionEnum.SIGN_IS_ERROR);
            }
        }catch (Exception e){
            throw new LyException(ExceptionEnum.SIGN_IS_ERROR);

        }
    }

    /**
     * 查询订单状态
     *
     * @param orderId
     * @return
     */
//    public PayState queryOrder(Long orderId) {
//        Map<String, String> data = new HashMap<>();
//        // 订单号
//        data.put("out_trade_no", orderId.toString());
//        try {
//            Map<String, String> result = this.wxPay.orderQuery(data);
//            if (result == null) {
//                // 未查询到结果，认为是未付款
//                return PayState.NOT_PAY;
//            }
//            String state = result.get("trade_state");
//            if ("SUCCESS".equals(state)) {
//                // success，则认为付款成功
//
//                // 修改订单状态
//                this.orderService.updateStatus(orderId, 2);
//                return PayState.SUCCESS;
//            } else if (StringUtils.equals("USERPAYING", state) || StringUtils.equals("NOTPAY", state)) {
//                // 未付款或正在付款，都认为是未付款
//                return PayState.NOT_PAY;
//            } else {
//                // 其它状态认为是付款失败
//                return PayState.FAIL;
//            }
//        } catch (Exception e) {
//            logger.error("查询订单状态异常", e);
//            return PayState.NOT_PAY;
//        }
//    }
}
