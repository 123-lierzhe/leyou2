package com.sjzc.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
@Data
@ConfigurationProperties("ly.worker")
public class IdWorkProperties {
    private long workerId;//当前机器id
    private Long dataCenterId;//序列号
}
