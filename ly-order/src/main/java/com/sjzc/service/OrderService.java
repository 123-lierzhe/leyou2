package com.sjzc.service;

import com.github.wxpay.sdk.WXPay;
import com.netflix.discovery.converters.Auto;
import com.sjzc.client.AddressClient;
import com.sjzc.client.GoodsClient;
import com.sjzc.domain.Sku;
import com.sjzc.dto.AddressDTO;
import com.sjzc.dto.CartDTO;
import com.sjzc.dto.OrderDTO;
import com.sjzc.entity.UserInfo;
import com.sjzc.enums.ExceptionEnum;
import com.sjzc.enums.OrderStatusEneum;
import com.sjzc.enums.PayState;
import com.sjzc.exception.LyException;
import com.sjzc.interceptor.UserInterceptor;
import com.sjzc.mapper.OrderDetailMapper;
import com.sjzc.mapper.OrderMapper;
import com.sjzc.mapper.OrderStatusMapper;
import com.sjzc.pojo.Order;
import com.sjzc.pojo.OrderDetail;
import com.sjzc.pojo.OrderStatus;
import com.sjzc.util.PayHelper;
import com.sjzc.utils.IdWorker;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
public class OrderService {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderDetailMapper orderDetailMapper;
    @Autowired
    private OrderStatusMapper orderStatusMapper;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private GoodsClient goodsClient;
    @Autowired
    private PayHelper payHelper;
    @Autowired
    private WXPay wxPay;

    public Long createOrder(OrderDTO orderDTO) {

        //新增订单
        Order order = new Order();

        //生成订单编号及基本信息
        long orderId = idWorker.nextId();
        order.setOrderId(orderId);
        order.setCreateTime(new Date());
        order.setPaymentType(orderDTO.getPaymentType());

        //获取当前用户
        UserInfo user = UserInterceptor.getUser();
        order.setUserId(user.getId());
        order.setBuyerNick(user.getUsername());
        order.setBuyerRate(false);

        //获取收货人信息
        AddressDTO add = AddressClient.findById(orderDTO.getAddressId());
        order.setReceiver(add.getName());
        order.setReceiverAddress(add.getAddress());
        order.setReceiverCity(add.getCity());
        order.setReceiverDistrict(add.getDistrict());
        order.setReceiverMobile(add.getPhone());
        order.setReceiverState(add.getState());
        order.setReceiverZip(add.getZipCode());

        //金额
        //将cartDTO转换成map，key为skuId，value为num
        Map<Long, Integer> numMap = orderDTO.getCarts().stream()
                .collect(Collectors.toMap(CartDTO::getSkuId, CartDTO::getNum));
        //获得map中的所有key
        Set<Long> skuIds = numMap.keySet();
        List<Sku> skuList = goodsClient.querySkuByIds(new ArrayList<>(skuIds));

        List<OrderDetail> details = new ArrayList<>();

        long totalPay = 0L;
        for (Sku sku : skuList) {
            totalPay += sku.getPrice() * numMap.get(sku.getId());

            //封装商品详情
            OrderDetail detail = new OrderDetail();
            detail.setImage(StringUtils.substringBefore(sku.getImages(),","));
            detail.setNum(numMap.get(sku.getId()));
            detail.setOrderId(orderId);
            detail.setOwnSpec(sku.getOwnSpec());
            detail.setPrice(sku.getPrice());
            detail.setSkuId(sku.getId());
            detail.setTitle(sku.getTitle());
            details.add(detail);

        }
        order.setTotalPay(totalPay);
        //实际付款
        order.setActualPay(totalPay+order.getPostFee()-0);
        //写入数据库
        int count = orderMapper.insertSelective(order);
        if(count != 1){
            log.info("【订单创建服务】失败,orderId:{}",orderId);
            throw new LyException(ExceptionEnum.CREATE_ORDER_ERROR);
        }

        //新增订单详情
        count=orderDetailMapper.insertList(details);
        if(count!=details.size()){
            log.info("【订单详情创建服务】失败,orderId:{}",orderId);
            throw new LyException(ExceptionEnum.CREATE_ORDER_ERROR);

        }

        //新增i订单状态
        OrderStatus status = new OrderStatus();
        status.setCreateTime(order.getCreateTime());
        status.setOrderId(orderId);
        status.setStatus(OrderStatusEneum.UN_PAY.value());
        count=orderStatusMapper.insert(status);
        if(count!=1){
            log.info("【订单状态创建服务】失败,orderId:{}",orderId);
            throw new LyException(ExceptionEnum.CREATE_ORDER_ERROR);

        }
        //减库存
         List<CartDTO> carts = orderDTO.getCarts();
        goodsClient.decreaseStock(carts);

        return orderId;
    }

    public Order queryOrderById(Long id) {
        //查询订单
        Order order = orderMapper.selectByPrimaryKey(id);
        if(order == null ){
            throw  new LyException(ExceptionEnum.ORDER_NOT_FOUND);
        }
        //查询订单详情
        OrderDetail detail = new OrderDetail();
        detail.setOrderId(id);
        List<OrderDetail> details = orderDetailMapper.select(detail);
        if(CollectionUtils.isEmpty(details)){
            throw  new LyException(ExceptionEnum.ORDER_DETAIL_NOT_FOUND);
        }
        order.setOrderDetails(details);
        //查询订单状态
        OrderStatus status = orderStatusMapper.selectByPrimaryKey(id);
        if(status == null ){
            throw  new LyException(ExceptionEnum.ORDER_STATUS_NOT_FOUND);
        }
        order.setOrderStatus(status);
        return order;

    }

    public String  createPayUrl(Long orderId) {
        Order order = queryOrderById(orderId);

        Integer orderStatus = order.getOrderStatus().getStatus();
        if(orderStatus != OrderStatusEneum.UN_PAY.value()){
            throw new LyException(ExceptionEnum.ORDER_STATUS_ERROR);
        }
        Long actualPay = /*order.getActualPay()*/ 1L;
        return payHelper.createPayUrl(orderId,actualPay);

    }

    public void handleNotify(Map<String, String> result) {
        if ("FAIL".equals(result.get("return_code"))) {
            //数据校验
            log.error("【微信下单】微信下单通信失败，失败原因：{}",result.get("return_msg"));
            throw new LyException(ExceptionEnum.WX_PAY_ORDER_FAIL);
        }

        if ("FAIL".equals(result.get("result_code"))) {
            log.error("【微信下单】微信下单业务失败");
            throw new LyException(ExceptionEnum.WX_PAY_ORDER_FAIL);
        }

        //签名校验
        payHelper.isValidSign(result);

        //校验金额
        String totalFeeStr = result.get("total_fee");
        String outTradeNoStr = result.get("out_trade_no");
        if(StringUtils.isBlank(totalFeeStr) || StringUtils.isBlank(outTradeNoStr)){
            throw new LyException(ExceptionEnum.INVALID_ORDER_FEE);
        }
        Long totalFee = Long.valueOf(totalFeeStr);
        Long orderId = Long.valueOf(outTradeNoStr);
        Order order = orderMapper.selectByPrimaryKey(orderId);
        if(totalFee != 1){
            throw new LyException(ExceptionEnum.INVALID_ORDER_FEE);
        }
        //修改订单状态
        OrderStatus status = new OrderStatus();
        status.setStatus(OrderStatusEneum.PAYED.value());
        status.setOrderId(orderId);
        status.setPaymentTime(new Date());
        int count=orderStatusMapper.updateByPrimaryKeySelective(status);
                if(count!=1){
                    throw new LyException(ExceptionEnum.UPDATE_STATUS_ERROR);
                }

    }

    public PayState queryOrderState(Long orderId) {
        try {
            //组织请求参数
            Map<String, String> data = new HashMap<>();
            //订单号
            data.put("out_trade_no", orderId.toString());
            //查询状态
            Map<String, String> result = wxPay.orderQuery(data);

            if ("FAIL".equals(result.get("return_code"))) {
                //数据校验
                log.error("【微信下单】微信下单通信失败，失败原因：{}", result.get("return_msg"));
                throw new LyException(ExceptionEnum.WX_PAY_ORDER_FAIL);
            }

            if ("FAIL".equals(result.get("result_code"))) {
                log.error("【微信下单】微信下单业务失败");
                throw new LyException(ExceptionEnum.WX_PAY_ORDER_FAIL);
            }

            //签名校验
            payHelper.isValidSign(result);

            //检验金额
            String totalFeeStr = result.get("total_fee");
            String tradeNo = result.get("out_trade_no");
            if (StringUtils.isEmpty(totalFeeStr) || StringUtils.isEmpty(tradeNo)) {
                throw new LyException(ExceptionEnum.INVALID_ORDER_FEE);
            }
            //获得结果中的金额
            Long totalFee = Long.valueOf(totalFeeStr);
            //获得订单金额
            Order order = orderMapper.selectByPrimaryKey(orderId);
            if (totalFee != 1) {
                throw new LyException(ExceptionEnum.INVALID_ORDER_FEE);

            }

            String state = result.get("trade_state");
            if ("SUCCESS".equals(state)) {
                //支付成功，修改订单状态
                OrderStatus status = new OrderStatus();
                status.setStatus(OrderStatusEneum.PAYED.value());
                status.setOrderId(orderId);
                status.setPaymentTime(new Date());
                int count = orderStatusMapper.updateByPrimaryKeySelective(status);
                if (count != 1) {
                    throw new LyException(ExceptionEnum.UPDATE_STATUS_ERROR);
                }
                return PayState.SUCCESS;
            }
            if ("NOTPAY".equals(state) || "USERPAYING".equals(state)) {
                return PayState.NOT_PAY;

            }
            return PayState.FAIL;
        }catch (Exception e){
            return PayState.NOT_PAY;
        }
    }

}
