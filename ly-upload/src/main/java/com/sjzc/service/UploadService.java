package com.sjzc.service;

import com.sjzc.enums.ExceptionEnum;
import com.sjzc.exception.LyException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class UploadService {

    private static final List<String> ALLOE_TYPES = Arrays.asList("image/jpeg","image/png","image/bmp");

    /**
     *图片上传
     *
     */
    public String  uploadImage(MultipartFile file) {
        try {
        //校验文件类型
        String contentType = file.getContentType();
        if(!ALLOE_TYPES.contains(contentType)){
            throw new LyException(ExceptionEnum.FILE_TYPE_NOT_MATCH);
        }
        //校验文件类型
        BufferedImage image = null;
        try {
            image = ImageIO.read(file.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(image == null){
            throw new LyException(ExceptionEnum.FILE_TYPE_NOT_MATCH);
        }
        //准备目标路径(图片上传后在本地的存储位置)
        File dest = new File("E:\\workSpace Idea\\uploadedImage\\",file.getOriginalFilename());
        //保存文件到本地

            file.transferTo(dest);
            //返回路径
            return "http://image.leyou.com/" + file.getOriginalFilename();

        } catch (IOException e) {
            log.error("图片上传失败",e);
            throw new LyException(ExceptionEnum.UPLOAD_IMAGE_ERROR);
        }



    }
}
