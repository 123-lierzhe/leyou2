package com.sjzc.service;

import com.sjzc.enums.ExceptionEnum;
import com.sjzc.exception.LyException;
import com.sjzc.mapper.UserMapper;
import com.sjzc.pojo.User;
import com.sjzc.utils.NumberUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private AmqpTemplate amqpTemplate;
    @Autowired
    private StringRedisTemplate redisTemplate;

    private static final String KEY_PREFIX="user:verify:phone:";

    public Boolean checkData(String data, Integer type) {
        User user = new User();
        switch (type) {
            case 1:
                user.setUsername(data);
                break;
            case 2:
                user.setPhone(data);
                break;
            default:
                throw new LyException(ExceptionEnum.DATA_TYPE_IS_INVALID);
        }
        int i = userMapper.selectCount(user);
        return i==0;
    }

    public void sendMessage(String phone) {
        String key=KEY_PREFIX+phone;
        //生成验证码
        String code = NumberUtils.generateCode(6);
        //发送消息
        Map<String,String> msg = new HashMap<>();
        msg.put("phone",phone);
        msg.put("code",code);
        amqpTemplate.convertAndSend("ly.sms.exchange","sms.verify.code",msg);
        //保存验证码
        redisTemplate.opsForValue().set(key,code,5, TimeUnit.MINUTES);

    }

    public void register(User user, String code) {
        //核对验证码
        String cacheCode = redisTemplate.opsForValue().get(KEY_PREFIX + user.getPhone());
        if(!StringUtils.equals(code,cacheCode)){
            throw new LyException(ExceptionEnum.INVALID_CODE);
        }
        // 加密为引入加密的工具类16-1
        String salt ="jkjkj";
        user.setSalt(salt);
        user.setPassword(DigestUtils.md5Hex(user.getPassword()+salt));
        //注册
        user.setCreated(new Date());
        userMapper.insert(user);
    }

    public User queryUserByUsernameAndPassword(String username, String password) {
        //查询
        User user = new User();
        user.setUsername(username);
        User selectOne = userMapper.selectOne(user);
        if(selectOne==null){
            throw new LyException(ExceptionEnum.INCALID_USERNAME_PASSWORD);
        }
        //校验密码
        if(!StringUtils.equals(selectOne.getPassword(),DigestUtils.md5Hex(password+selectOne.getSalt()))){
            throw new LyException(ExceptionEnum.INCALID_USERNAME_PASSWORD);
        }
        return selectOne;
    }
}
