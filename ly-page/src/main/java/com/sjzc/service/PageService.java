package com.sjzc.service;

import com.sjzc.client.BrandClient;
import com.sjzc.client.CategoryClient;
import com.sjzc.client.GoodsClient;
import com.sjzc.client.SpecificationClient;
import com.sjzc.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.File;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PageService {
    @Autowired
    private BrandClient brandClient;
    @Autowired
    private CategoryClient categoryClient;
    @Autowired
    private GoodsClient goodsClient;
    @Autowired
    private SpecificationClient specificationClient;
    @Autowired
    private TemplateEngine templateEngine;

    public Map<String, Object> loadModel(Long spuId) {
        Map<String,Object> model = new HashMap<>();
        Spu spu = goodsClient.querySpuById(spuId);
        List<Sku> skus = spu.getSkus();
        SpuDetail spuDetail = spu.getSpuDetail();
        Brand brand = brandClient.queryBrandById(spu.getBrandId());
        List<Category> categories = categoryClient.queryCategoryListById(
                Arrays.asList(spu.getCid1(), spu.getCid2(), spu.getCid3()));
        List<SpecGroup> specs = specificationClient.queryGroupByCid(spu.getCid3());
        model.put("spu",spu);
        model.put("skus",skus);
        model.put("detail",spuDetail);
        model.put("brand",brand);
        model.put("categories",categories);
        model.put("specs",specs);
        return model;

    }

    public void createHtml(Long spuId) throws Exception{
        //上下文
        Context context = new Context();
        context.setVariables(loadModel(spuId));
        //输出流
        File file = new File("D:\\html", spuId + ".html");
        if(file.exists()){
            file.delete();
        }
        PrintWriter writer = new PrintWriter(file, "UTF-8");
        //生成html
        templateEngine.process("item",context,writer);
    }

    public void deleteIndex(Long spuId) {
        File file = new File("D:\\html", spuId + ".html");
        if(file.exists()){
            file.delete();
        }
    }
}
