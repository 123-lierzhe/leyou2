package com.sjzc.exception;

import com.sjzc.enums.ExceptionEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class LyException extends RuntimeException {
    //获得异常
    private ExceptionEnum exceptionEnum;
}
