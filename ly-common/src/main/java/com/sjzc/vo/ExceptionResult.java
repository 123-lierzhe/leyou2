package com.sjzc.vo;

import com.sjzc.enums.ExceptionEnum;
import lombok.Data;

@Data
public class ExceptionResult {
    private int status;
    private String message;
    private Long timeStamp;

    public ExceptionResult(ExceptionEnum exceptionEnum) {
        this.status=exceptionEnum.getCode();
        this.message=exceptionEnum.getMsg();
        timeStamp=System.currentTimeMillis();
    }
}
