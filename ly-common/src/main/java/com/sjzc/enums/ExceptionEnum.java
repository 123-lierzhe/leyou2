package com.sjzc.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
//提供getter方法，有参构造器以及无参构造器
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum ExceptionEnum {
    //new出一个实例，其中包含如下状态码和错误
   AGE_OR_NAME_CAN_NOT_NULL(401,"年龄和姓名不能为空"),
    CATEGORY_LIST_ISEMPTY(404,"商品分类未查到"),
    BRANDS_NOT_FOUND(404,"品牌分类未找到"),
    GROUP_IS_NOT_FOUND(404,"商品规格组名未找到"),
    PARAM_IS_NOT_FOUND(404,"组内参数未找到"),
    GOODS_NOT_FIND(404,"商品未找到"),
    FIND_FETAILS_ERROR(404,"查询商品详情失败"),
    FIND_SKU_ERROR(404,"查询商品sku失败"),
    FIND_STOCK_ERROR(404,"查询库存失败"),
    GOODS_ID_IS_NOT_FIND(404,"商品的id未找到"),
    SKU_IS_NOT_FIND(404,"sku未找到"),
    SAVE_BRAND_FAILED(500,"新增品牌失败"),
    SAVE_GOODS_ERROR(500,"新增商品失败"),
    UPLOAD_IMAGE_ERROR(500,"上传图片失败"),
    UPDATE_SPU_ERROR(500,"更新商品失败"),
    FILE_TYPE_NOT_MATCH(400,"文件类型不铺匹配"),
    DATA_TYPE_IS_INVALID(400,"用户数据类型无效"),
    INVALID_CODE(400,"无效的验证码"),
    INCALID_USERNAME_PASSWORD(400,"用户名或密码错误"),
     UNAUTHORIZED(403,"用户未授权"),
    CART_NOT_FOUND(403,"用户未授权"),
    CREATE_ORDER_ERROR(500,"订单创建失败"),
    STOCH_NOT_ENOUGH(500,"库存不足"),
    ORDER_NOT_FOUND(404,"订单不存在"),
    ORDER_DETAIL_NOT_FOUND(404,"订单不存在"),
    ORDER_STATUS_NOT_FOUND(404,"订单状态不存在"),
   ORDER_STATUS_ERROR(400,"订单状态异常"),
    WX_PAY_ORDER_FAIL(500,"微信下单失败"),
   SIGN_IS_ERROR(400,"签名异常"),
 INVALID_ORDER_FEE(400,"订单金额异常"),
 UPDATE_STATUS_ERROR(400,"修改订单状态异常"),











 ;
 //定义该异常的属性
    private int code;
    private String msg;
}
